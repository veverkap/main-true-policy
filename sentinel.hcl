policy "always-true" {
  source = "./always-true.sentinel"
  enforcement_level = "advisory"
}